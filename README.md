A simple resume template in TeX
-------------------------------

This resume template is inspired (stolen) from [SimpleResumeTemplate](http://sampleresumetemplate.net).

** [Preview](https://github.com/lamnk/srt-tex/raw/master/template.pdf) **

Most resume package in CTAN don't look so visually appealing (this is subjective), so I stole the design from [SimpleResumeTemplate](http://sampleresumetemplate.net) and composed the template from scratch in LaTeX.

A template with photo is included for Europe applicants: in Germany you are _strongly_ recommended to put a photo in your resume, although not required by law.

To compile the template you'll need a modern TeX distribution that includes XeLaTex.

* Mac OS X: [MacTeX](http://www.tug.org/mactex/)
* Linux: [TeX Live](http://en.wikipedia.org/wiki/TeX_Live), it's recommended to install from your distro's package management system
* Windows: I don't use Windows, but heard that [MiKTeX](http://miktex.org/) should do the job


Pull requests are welcomed!


LICENSE
-------
(The MIT License)

Copyright (c) 2011 Ky Lam Ngo lamnk@lamnk.com

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the 'Software'), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED 'AS IS', WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
